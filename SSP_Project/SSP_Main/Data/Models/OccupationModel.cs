﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace SSP_Definition.Models
{
    [XmlRoot("Occupations")]
	public class Occupations
	{
        private PropertyInfo[] _PropertyInfos = null;
        [XmlElement("Occupation")]
		public List<Occupation> Items { get; set; }

        public Occupations() { Items = new List<Occupation>(); }

        public override string ToString()
        {
            if (_PropertyInfos == null)
                _PropertyInfos = this.GetType().GetProperties();

            var sb = new StringBuilder();

            foreach (var info in _PropertyInfos)
            {
                var value = info.GetValue(this, null) ?? "(null)";
                sb.AppendLine(info.Name + ": " + value.ToString());
            }

            return sb.ToString();
        }
    }

	[XmlRoot("Occupation")]
    public class Occupation
	{
        private PropertyInfo[] _PropertyInfos = null;
        [XmlElement("Desc")]
		public String RiskRole { get; set; }

		[XmlElement("Action")]
		public String RiskAction { get; set; }

        public override string ToString()
        {
            if (_PropertyInfos == null)
                _PropertyInfos = this.GetType().GetProperties();

            var sb = new StringBuilder();

            foreach (var info in _PropertyInfos)
            {
                var value = info.GetValue(this, null) ?? "(null)";
                sb.AppendLine(info.Name + ": " + value.ToString());
            }

            return sb.ToString();
        }
    }
}
