﻿﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace SSP_Definition.Models
{

    [XmlRoot("Risks")]
    public class Risks
    {
        private PropertyInfo[] _PropertyInfos = null;

        [XmlElement("Risk")]
        public List<Risk> Items { get; set; }

        public Risks() { Items = new List<Risk>(); }

        public override string ToString()
        {
            if (_PropertyInfos == null)
                _PropertyInfos = this.GetType().GetProperties();

            var sb = new StringBuilder();

            foreach (var info in _PropertyInfos)
            {
                var value = info.GetValue(this, null) ?? "(null)";
                sb.AppendLine(info.Name + ": " + value.ToString());
            }

            return sb.ToString();
        }
    }

    [XmlRoot("Risk")]
    public class Risk
    {
        private PropertyInfo[] _PropertyInfos = null;

        [XmlElement("Name")]
        public String RiskName { get; set; }

		[XmlElement("Occupation")]
		public String RiskOccupation { get; set; }

        [XmlElement("Address")]
        public Address Address { get; set; }

        [XmlElement("KeptPostcode")]
        public String KeptAddress { get; set; }

        public override string ToString()
        {
            if (_PropertyInfos == null)
                _PropertyInfos = this.GetType().GetProperties();

            var sb = new StringBuilder();

            foreach (var info in _PropertyInfos)
            {
                var value = info.GetValue(this, null) ?? "(null)";
                sb.AppendLine(info.Name + ": " + value.ToString());
            }

            return sb.ToString();
        }
    }

    [XmlRoot("Address")]
    public class Address{
        private PropertyInfo[] _PropertyInfos = null;

        [XmlElement("Address1")]
		public String AddressLineOne { get; set; }

		[XmlElement("Postcode")]
		public String AddressPostCode { get; set; }

        public override string ToString()
        {
            if (_PropertyInfos == null)
                _PropertyInfos = this.GetType().GetProperties();

            var sb = new StringBuilder();

            foreach (var info in _PropertyInfos)
            {
                var value = info.GetValue(this, null) ?? "(null)";
                sb.AppendLine(info.Name + ": " + value.ToString());
            }

            return sb.ToString();
        }
    }
}