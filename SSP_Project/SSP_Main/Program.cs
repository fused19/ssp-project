﻿using SSP_Definition.Interfaces;
using SSP_Definition.Implementation;
using SSP_Definition;
using SSP_Definition.Models;
using System;
using System.Configuration;

namespace SSP_Implementation
{
    class Program
    {
        static void Main(string[] args)
        {
            try {
            IXmlReader xmlDataReader = new XmlReader();
            IWriter fileWriter = new FileWriter(ConfigurationManager.AppSettings["myFilePath"]);
            IValidator validator = new Validator(ConfigurationManager.AppSettings["myFilePath"]);

            Dependencies dependencies = new Dependencies(xmlDataReader, fileWriter, validator);

            new FileHandle(dependencies).Run();
            } catch (Exception ex) {
                Environment.Exit(1);
            }
        }
    }
}