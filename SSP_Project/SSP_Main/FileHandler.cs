﻿using System;
using SSP_Definition.Interfaces;
using SSP_Definition.Implementation;
using SSP_Main.Domain.BusinessLogic;
using SSP_Definition.Domain.BusinessLogic;
using NLog;
using System.Reflection;
using System.Configuration;
using SSP_Definition.Models;

namespace SSP_Definition
{
    public class FileHandle
    {
        private readonly IXmlReader XmlReader;
        private readonly IWriter Writer;
        private readonly IValidator Validator;
        private readonly Logger logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType.ToString());
        private readonly string sessionId;

        public FileHandle(Dependencies dependencies)
        {
            sessionId = GetSessionId();
            XmlReader = dependencies.XmlReader;
            Writer = dependencies.Writer;
            Validator = dependencies.Validator;
        }

        private static string GetSessionId()
        {
            DateTime date = DateTime.Now;
            return date.ToString("yyyyMMdd");
        }

        public void Run()
        {
            try
            {
                Start();
                TestFunctionality();
            }
            catch (Exception generalException)
            {
                logger.Error("Session Id ({0}) => Failed for the following reason: {1}.\n STACK TRACE\n {2}", sessionId, generalException.Message, generalException.StackTrace);
            }
        }

        public void Start()
        {
            LogManager.ThrowExceptions = true;
            logger.Info("******************************************************************************");
            logger.Info("Session Id ({0}) => Starting.", sessionId);
        }

        private void End()
        {
            logger.Info("Session Id ({0}) => Finishing.", sessionId);
            logger.Info("******************************************************************************");
        }

        private void TestFunctionality()
        {
            int cnt = 1;
            
            var returnRiskOccuptation = ReturnOccupationAction.GetOccupations(XmlReader);

            var risks = ReturnRisks.GetRisks(XmlReader);

            var postCodeChecker = new PostCodeChecker();

            var checkDistance = postCodeChecker.CheckDistance(risks);

            logger.Info("Session Id ({0}) Creating the files", sessionId);

            Writer.WriteFile<Risk>(risks, @"Risk.txt");
        }

    }
}