﻿using System.Collections.Generic;

namespace SSP_Definition.Interfaces
{
    public interface IValidator
    {
        List<T> ValidateXml<T>(List<T> value);
    }
}