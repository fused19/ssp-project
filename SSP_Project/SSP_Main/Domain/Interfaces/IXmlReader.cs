﻿using SSP_Definition.Models;
using System.Collections.Generic;

namespace SSP_Definition.Interfaces
{
    public interface IXmlReader
    {
        T ReadXml<T>(string path);

        List<T> ReadListXml<T>(string path, string root);
    }
}