﻿using SSP_Definition.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SSP_Definition.Interfaces
{
    interface IPostCodeChecker
    {
        List<KeyValuePair<string, string>> CheckDistance(List<Risk> risks);
    }
}
