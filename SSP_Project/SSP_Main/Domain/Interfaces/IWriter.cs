﻿using System.Collections.Generic;

namespace SSP_Definition.Interfaces
{
    public interface IWriter
    {
        void WriteFile<T>(List<T> items, string fileName);
    }
}