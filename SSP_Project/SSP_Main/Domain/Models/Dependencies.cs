﻿using SSP_Definition.Interfaces;

namespace SSP_Definition.Models
{
    public class Dependencies
    {
        public IXmlReader XmlReader { get; private set; }
        public IWriter Writer { get; private set; }
        public IValidator Validator { get; private set; }

        public Dependencies(IXmlReader xmlReader, IWriter writer, IValidator validator)
        {
            this.XmlReader = xmlReader;
            this.Writer = writer;
            this.Validator = validator;
        }
    }
}
