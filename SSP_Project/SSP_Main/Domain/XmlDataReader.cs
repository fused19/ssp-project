﻿using System;
using SSP_Definition.Interfaces;
using SSP_Definition.Models;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace SSP_Definition.Implementation
{
    public class XmlReader: IXmlReader
    {
        public List<T> ReadListXml<T>(string path, string root)
        {
            List<T> ItemList = default(List<T>);
            if (string.IsNullOrEmpty(path)) return default(List<T>);

            XmlSerializer serializer = new XmlSerializer(typeof(List<T>), new XmlRootAttribute(root));
            using (StreamReader xmlStream = new StreamReader(path))
            {
                ItemList = (List<T>)serializer.Deserialize(xmlStream);
            }
                return ItemList;
        }

        public T ReadXml<T>(string path)
        {
			T returnObject = default(T);
			if (string.IsNullOrEmpty(path)) return default(T);

			try
			{
				StreamReader xmlStream = new StreamReader(path);
				XmlSerializer serializer = new XmlSerializer(typeof(T));
                
				returnObject = (T)serializer.Deserialize(xmlStream);
			}
			catch (Exception ex)
			{
				
			}
			return returnObject;
        }
    }
}
