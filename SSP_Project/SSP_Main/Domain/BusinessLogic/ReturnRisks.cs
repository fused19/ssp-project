﻿using SSP_Definition.Interfaces;
using SSP_Definition.Models;
using System;
using System.Collections.Generic;
using System.IO;

namespace SSP_Main.Domain.BusinessLogic
{
    public class ReturnRisks
    {
        public static List<Risk> GetRisks(IXmlReader XmlReader)
        {
            var xmlReader = XmlReader;
            var riskPath = Path.GetFullPath(Path.Combine(AppContext.BaseDirectory, @"../../../SSP_Main/XmlFiles/Risk.xml"));
            var risks = xmlReader.ReadListXml<Risk>(riskPath.ToString(), "Risks");
            return risks;
        }

        public static List<Risk> GetRisks(IXmlReader XmlReader, string path)
        {
            var xmlReader = XmlReader;
            var riskPath = Path.GetFullPath(Path.Combine(AppContext.BaseDirectory, path));
            var risks = xmlReader.ReadListXml<Risk>(riskPath.ToString(), "Risks");
            return risks;
        }
    }
}
