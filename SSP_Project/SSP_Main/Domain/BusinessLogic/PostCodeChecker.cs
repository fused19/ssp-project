﻿using SSP_Definition.Interfaces;
using MarkEmbling.PostcodesIO;
using SSP_Definition.Models;
using System.Device.Location;
using System.Collections.Generic;
using System;

namespace SSP_Definition.Implementation
{

    public class PostCodeChecker : IPostCodeChecker
    {
        public List<KeyValuePair<string, string>> CheckDistance(List<Risk> risks)
        {
            var results = new List<KeyValuePair<string, string>>();
            try
            {
                foreach (var risk in risks)
                {
                    double distanceBetween = CalculateDistance(risk);
                    if (distanceBetween <= 10)
                    {
                        results.Add(new KeyValuePair<string, string>(risk.RiskName, "Accept"));
                    }
                    else if (distanceBetween > 10 && distanceBetween <= 100)
                    {
                        results.Add(new KeyValuePair<string, string>(risk.RiskName, "Refer"));
                    }
                    else if (distanceBetween > 100)
                    {
                        results.Add(new KeyValuePair<string, string>(risk.RiskName, "Decline"));
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return results;
        }

        private double CalculateDistance(Risk risk)
        {
            string carLoc = risk.KeptAddress == "" ? risk.Address.AddressPostCode : risk.KeptAddress;
            var client = new PostcodesIOClient();
            var home = client.Lookup(carLoc);
            var kept = client.Lookup(risk.Address.AddressPostCode);
            GeoCoordinate homeGeo = new GeoCoordinate(home.Latitude, home.Longitude);
            GeoCoordinate keptGeo = new GeoCoordinate(kept.Latitude, kept.Longitude);
            double distanceBetween = homeGeo.GetDistanceTo(keptGeo);
            return distanceBetween;
        }
    }
}
