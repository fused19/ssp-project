﻿using System;
using System.IO;
using System.Collections.Generic;
using SSP_Definition.Models;
using SSP_Definition.Interfaces;

namespace SSP_Definition.Domain.BusinessLogic
{
    public static class ReturnOccupationAction
    {
        public static List<KeyValuePair<string, string>> GetOccupations(IXmlReader XmlReader)
        {
            var xmlReader = XmlReader;
            var riskPath = Path.GetFullPath(Path.Combine(AppContext.BaseDirectory, @"../../../SSP_Main/XmlFiles/Risk.xml"));
            var risks = xmlReader.ReadListXml<Risk>(riskPath.ToString(), "Risks");

            var occupationPath = Path.GetFullPath(Path.Combine(AppContext.BaseDirectory, @"../../../SSP_Main/XmlFiles/Occupations.xml"));
            var occupations = xmlReader.ReadListXml<Occupation>(occupationPath.ToString(), "Occupations");

            var results = new List<KeyValuePair<string, string>>();
            var action = "Accept";
            foreach (var risk in risks)
            {
                var sq = occupations.Find(c => c.RiskRole == risk.RiskOccupation) ?? new Occupation();
                action = sq.RiskAction;

                if (sq.RiskAction == null)
                    action = "Decline";

                results.Add(new KeyValuePair<string, string>(risk.RiskName, action));
            }
            return results;
        }
    }
}
