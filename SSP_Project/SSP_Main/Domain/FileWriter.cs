﻿using SSP_Definition.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.IO;


namespace SSP_Definition.Implementation
{
    public class FileWriter : IWriter
    {
        private string filePath;

        public FileWriter(string filePath)
        {
            this.filePath = filePath;
        }

        public void WriteFile<T>(List<T> items, string fileName)
        {
            List<T> Items = items;
            if (Items.Count > 0)
            {
                WriteFile(fileName, Items);
            }
        }

        private void WriteFile<T>(string file, List<T> Items)
        {
            VerifyDirectory(filePath);

            bool appendIfFileExists = false;

            using (StreamWriter writer = new StreamWriter(filePath + file, appendIfFileExists))
            {
                WriteHeaderInfo(writer);
                WriteRiskInfo<T>(writer, Items);
            }
        }

        private void VerifyDirectory(string filePath)
        {
            if (!Directory.Exists(filePath))
                Directory.CreateDirectory(filePath);
        }

        private void WriteHeaderInfo(StreamWriter writer)
        {
            Write(writer, new string[] { "[Root- Test]" });
        }

        private void WriteRiskInfo<T>(StreamWriter writer, List<T> items)
        {
            List<string> Risknfo = new List<string> { "[Risks]","[Test Data]", string.Empty };
            foreach (T branch in items)
            {
                var objectToString = branch.ToString();
                Risknfo.Add($"Risk-{objectToString}");
            }
            Write(writer, Risknfo.ToArray());
        }

        private void Write(StreamWriter writer, string[] items)
        {
            for (var i = 0; i < items.Length; i++)
            {
                writer.WriteLine(items[i]);
            }
        }
    }
}
