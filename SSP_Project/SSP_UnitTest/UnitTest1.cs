﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SSP_Definition.Interfaces;
using SSP_Definition.Domain.BusinessLogic;
using SSP_Definition.Models;
using SSP_Definition.Implementation;
using System.IO;
using System.Linq;
using SSP_Main.Domain.BusinessLogic;

namespace SSP_UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void ReturnOccupationActionTest()
        {
            //arrange
            List<KeyValuePair<string, string>> occList = new List<KeyValuePair<string, string>>();
            Assert.IsTrue(occList != null);

            //act
            var xmlFile = new XmlReader().ReadXml<Occupations>(@"../../../SSP_Main/XmlFiles/Occupations.xml");
            var xmlReader = new XmlReader();
            occList = ReturnOccupationAction.GetOccupations(xmlReader);

            //assert
            Assert.IsTrue(occList.Count > 0);
        }

        [TestMethod]
        public void ReturnRisksTest()
        {
            //arrange
            List<Risk> riskList = new List<Risk>();
            Assert.IsTrue(riskList != null);

            //act
            var xmlFile = new XmlReader().ReadXml<Occupations>(@"../../../SSP_Main/XmlFiles/Risk.xml");
            var xmlReader = new XmlReader();
            riskList = SSP_Main.Domain.BusinessLogic.ReturnRisks.GetRisks(xmlReader);

            //assert
            Assert.IsTrue(riskList.Count > 0);
        }

        [TestMethod]
        public void RiskOccupationActionTest()
        {
            //arrange
            List<KeyValuePair<string, string>> occupationActionList = new List<KeyValuePair<string, string>>();
            Assert.IsTrue(occupationActionList != null);
            Assert.IsFalse(occupationActionList.Exists(c => c.Value == "Accept") || occupationActionList.Exists(c => c.Value == "Decline") || occupationActionList.Exists(c => c.Value == "Refer"));

            //act
            var xmlReader = new XmlReader();

            var riskPath = Path.GetFullPath(Path.Combine(AppContext.BaseDirectory, @"../../../SSP_Main/XmlFiles/Risk.xml"));
            var risks = xmlReader.ReadListXml<Risk>(riskPath.ToString(), "Risks");

            var occupationPath = Path.GetFullPath(Path.Combine(AppContext.BaseDirectory, @"../../../SSP_Main/XmlFiles/Occupations.xml"));
            var occupations = xmlReader.ReadListXml<Occupation>(occupationPath.ToString(), "Occupations");

            occupationActionList = ReturnOccupationAction.GetOccupations(xmlReader);

            //assert
            Assert.IsTrue(occupationActionList.Count > 0);
            Assert.IsTrue(occupationActionList.Exists(c => c.Value == "Accept") || occupationActionList.Exists(c => c.Value == "Decline") || occupationActionList.Exists(c => c.Value == "Refer"));
        }

        [TestMethod]
        public void PostCodeDistanceCalculationTest()
        {
            //arrange
            List<KeyValuePair<string, string>> postCodeCheck = new List<KeyValuePair<string, string>>();
            Assert.IsTrue(postCodeCheck != null);

            //act
            var xmlReader = new XmlReader();
            var risks = ReturnRisks.GetRisks(xmlReader, @"../../../SSP_UnitTest/XmlTest/TestData.xml");
            var postCodeChecker = new PostCodeChecker();

            postCodeCheck = postCodeChecker.CheckDistance(risks);
            Assert.IsTrue(postCodeCheck != null);

            //assert
            Assert.IsTrue(postCodeCheck.ElementAt(0).Value == "Accept" && postCodeCheck.ElementAt(1).Value == "Refer" && postCodeCheck.ElementAt(2).Value == "Decline");
        }
    }
}
